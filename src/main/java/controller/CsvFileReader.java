package controller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import model.FootballData;
import model.WeatherData;

public class CsvFileReader {
	
	private String path;
	private List<Object> filteredWeatherData = new ArrayList<>();
	private List<Object> filteredFootballData = new ArrayList<>();

	/*
	 * Method to read a (CSV) file
	 * @return: A List of Strings of all lines included in the file
	 */
	public List<String> readFile(String type){
	
		List<String> dataFromCsv = new ArrayList<>();
		
		if(type.equals("WeatherData")) {
			path = "src/resources/weather.csv";
		} else if(type.equals("FootballData")) {
			path = "src/resources/football.csv";
		} else {
			System.out.println("Ups! The input of the datatype was wrong!");
			return null;
		}
		try (BufferedReader br = new BufferedReader(new FileReader(path))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		    	dataFromCsv.add(line);
		    }
		    br.close();
		} catch (Exception e) {
			System.out.println("Ups! Something went wrong while reading the file!");
			System.out.println(e.getMessage());
		}
		
		return dataFromCsv;
	}
	
	/*
	 * Method to filter the weather data for number of day(place 0), min temperature (place 2) and max temperature (place 1)
	 * 
	 * @param allData: A List of read data from a file (see readFile())
	 * @return: A List of Object (WeatherData)
	 */
	public List<Object> filterCsvDataWeather(List<String> allData){
		for(int i = 1; i < allData.size(); i++) {
			String[] splittedData = allData.get(i).split(",");
			try {
				filteredWeatherData.add(new WeatherData(Integer.parseInt(splittedData[0]), Integer.parseInt(splittedData[2]), Integer.parseInt(splittedData[1])));
			} catch (Exception e) {
				System.out.println("Ups! Something went wrong while parsing String to Integer!");
				System.out.println(e.getMessage());
			}
		}
		return filteredWeatherData;
	}
	
	/*
	 * Method to filter the football data for team name (place 0), goals scored (place 5) and goals against (6)
	 * 
	 * @param allData: A List of read data from a file (see readFile())
	 * @return: A List of Object (FootballData)
	 */
	public List<Object> filterCsvDataFootball(List<String> allData){
		for(int i = 1; i < allData.size(); i++) {
			String[] splittedData = allData.get(i).split(",");
			try {
				filteredFootballData.add(new FootballData(splittedData[0], Integer.parseInt(splittedData[5]), Integer.parseInt(splittedData[6])));
			} catch (Exception e) {
				System.out.println("Ups! Something went wrong while parsing String to Integer!");
				System.out.println(e.getMessage());
			}
		}
		return filteredFootballData;
	}
}
