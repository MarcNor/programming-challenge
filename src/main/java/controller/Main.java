package controller;

import java.util.List;

import model.FootballData;
import model.WeatherData;
import view.Output;

public class Main 
{
	
    public static void main( String[] args )
    {
    	CsvFileReader reader = new CsvFileReader();
    	WeatherData dayWithMinimumAmplitude = new WeatherData();
    	FootballData teamWithSmallestGoalDifference = new FootballData();
    	Calculator calculator = new Calculator();
    	Output output = new Output();
    	
    	//Analyze Weather Data
    	List<String> allDataWeather = reader.readFile("WeatherData");
    	List<Object> allFilteredDataWeather = reader.filterCsvDataWeather(allDataWeather);
    	
    	dayWithMinimumAmplitude = (WeatherData) calculator.analyseWeatherData(allFilteredDataWeather);
    	output.outputDayWithMinimumAmplitude(dayWithMinimumAmplitude);
    	
    	//Analyze Football Data
    	List<String> allDataFootball = reader.readFile("FootballData");
    	List<Object> allFilteredDataFootball = reader.filterCsvDataFootball(allDataFootball);
    	
    	teamWithSmallestGoalDifference = (FootballData) calculator.analyseFootballData(allFilteredDataFootball);
    	output.outputTeamWithSmallesGoalDifference(teamWithSmallestGoalDifference);
    }
}
