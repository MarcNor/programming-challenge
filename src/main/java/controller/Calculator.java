package controller;

import java.util.List;

import model.FootballData;
import model.WeatherData;

public class Calculator{
	
	/*
	 * Method to analyze the weatherdata and compare the amplitude between the data
	 * 
	 * @Param: List of objects of all weatherdata as read from the file
	 * @return: Weatherdata object with the smallest amplitude
	 */
	public Object analyseWeatherData(List<Object> allWeatherData) {
		int amplitude;
		int amplitudeForComparing = 0;
		WeatherData weatherDatawithLowestAmplitude = new WeatherData();
		
		for(int i = 0; i < allWeatherData.size(); i++) {
			WeatherData weatherData = new WeatherData();
			weatherData = (WeatherData) allWeatherData.get(i);
			amplitude = calculateDifference(weatherData.getMinTemp(), weatherData.getMaxTemp());
			if(amplitudeForComparing == 0 || amplitude < amplitudeForComparing) {
				amplitudeForComparing = amplitude;
				weatherData.setAmplitude(amplitude);
				weatherDatawithLowestAmplitude = weatherData;
			}
		}
		return weatherDatawithLowestAmplitude;
	}
	
	/*
	 * Method to analyze the footballData and compare the goal difference between the teams
	 * 
	 * @Param: List of objects of all footballData as read from the file
	 * @return: FootballData object with the smallest difference
	 */
	public Object analyseFootballData(List<Object> allFootballData) {
		int difference;
		int differenceForComparing = 0;
		FootballData FootballDatawithSmallestDifference = new FootballData();
		
		for(int i = 0; i < allFootballData.size(); i++) {
			FootballData footballData = new FootballData();
			footballData = (FootballData) allFootballData.get(i);
			difference = calculateDifference(footballData.getGoalsAgainst(), footballData.getGoalsScored());
			if(differenceForComparing == 0 || Math.abs(difference) < differenceForComparing) {
				differenceForComparing = Math.abs(difference);
				footballData.setDifference(Math.abs(difference));
				FootballDatawithSmallestDifference = footballData;
			}
		}
		return FootballDatawithSmallestDifference;
	}
	
	/*
	 * Method to calculate the difference
	 * 
	 * @Param: Integers for the minimum (data1) and the maximum (data2) Temperature of one day / goals scored (data2) and goals against (data1)
	 * @return: Difference between the Integers
	 */
	public int calculateDifference(int data1, int data2) {
		int amplitude;
		amplitude =  data2 - data1;
		return amplitude;
	}
}