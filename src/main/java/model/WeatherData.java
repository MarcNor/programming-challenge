package model;

public class WeatherData{
	private int numberOfDay;
	private int minTemp;
	private int maxTemp;
	private int amplitude;
	
	public WeatherData() {
	}
	
	public WeatherData(Integer day, Integer temp1, Integer temp2) {
		numberOfDay = day;
		minTemp = temp1;
		maxTemp = temp2;
	}
	
	public int getNumberOfDay() {
		return numberOfDay;
	}
	public void setNumberOfDay(int numberOfDay) {
		this.numberOfDay = numberOfDay;
	}
	
	public int getMinTemp() {
		return minTemp;
	}
	public void setMinTemp(int minTemp) {
		this.minTemp = minTemp;
	}
	
	public int getMaxTemp() {
		return maxTemp;
	}
	public void setMaxTemp(int maxTemp) {
		this.maxTemp = maxTemp;
	}
	
	public int getAmplitude() {
		return amplitude;
	}
	public void setAmplitude(int amplitude) {
		this.amplitude = amplitude;
	}
}