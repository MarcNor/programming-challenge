package model;

public class FootballData{
	private String teamname;
	private int goalsScored;
	private int goalsAgainst;
	private int difference;
	
	public FootballData() {
	}
	
	public FootballData(String teamname, Integer goalsScored, Integer goalsAgainst) {
		this.setTeamname(teamname);
		this.setGoalsScored(goalsScored);
		this.setGoalsAgainst(goalsAgainst);
	}

	public String getTeamname() {
		return teamname;
	}
	public void setTeamname(String teamname) {
		this.teamname = teamname;
	}

	public int getGoalsScored() {
		return goalsScored;
	}
	public void setGoalsScored(int goalsScored) {
		this.goalsScored = goalsScored;
	}

	public int getGoalsAgainst() {
		return goalsAgainst;
	}
	public void setGoalsAgainst(int goalsAgainst) {
		this.goalsAgainst = goalsAgainst;
	}

	public int getDifference() {
		return difference;
	}
	public void setDifference(int difference) {
		this.difference = difference;
	}
}
	