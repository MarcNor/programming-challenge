package view;

import model.FootballData;
import model.WeatherData;

public class Output{
	
	public void outputDayWithMinimumAmplitude(Object day) {
		
		WeatherData dayWithLowestAmplitude = new WeatherData();
		dayWithLowestAmplitude = (WeatherData) day;
		String NumberOfDayWithLowestAmplitude = Integer.toString(dayWithLowestAmplitude.getNumberOfDay());
		System.out.println("The day with the smallest amplitude is day " + NumberOfDayWithLowestAmplitude);
	}
	
	public void outputTeamWithSmallesGoalDifference(Object Team) {
		
		FootballData teamWithSmallestGoalDifference = new FootballData();
		teamWithSmallestGoalDifference = (FootballData) Team;
		String teamnameWithSmallestGoalDifference = teamWithSmallestGoalDifference.getTeamname();
		System.out.println("The team with the smallest goal difference is " + teamnameWithSmallestGoalDifference);
	}
}