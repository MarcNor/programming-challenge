package MarcNor;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import controller.Calculator;
import model.FootballData;
import model.WeatherData;

public class CalculatorTest 
{
    @Test
    //Test if calculation of amplitude works correctly
    public void calculateAmplitude()
    {
    	Calculator cal = new Calculator();
		int res = cal.calculateDifference(10, 25);
		assertEquals(15, res);
    }
    
    @Test
    //Test if the right day with the lowest amplitude will be chosen
    public void testWeatherData() {
        WeatherData wd1 = new WeatherData(1,10,25);
        WeatherData wd2 = new WeatherData(2,12,15);
        WeatherData wd3 = new WeatherData(3,8,16);
        List<Object> allWeatherData = new ArrayList<>();
        allWeatherData.add(wd1);
        allWeatherData.add(wd2);
        allWeatherData.add(wd3);
        WeatherData wdRes = new WeatherData();
        Calculator cal = new Calculator();
        wdRes = (WeatherData) cal.analyseWeatherData(allWeatherData);
        assertEquals(2, wdRes.getNumberOfDay());
        assertEquals(15, wdRes.getMaxTemp());
        assertEquals(12, wdRes.getMinTemp());
        assertEquals(3, wdRes.getAmplitude());
    }
    
    @Test
    //Test if the right team with the smallest goal difference will be chosen
    public void testFootballData() {
        FootballData fd1 = new FootballData("Team 1",55,49);
        FootballData fd2 = new FootballData("Team 2",51,59);
        FootballData fd3 = new FootballData("Team 3",67,42);
        List<Object> allFootballData = new ArrayList<>();
        allFootballData.add(fd1);
        allFootballData.add(fd2);
        allFootballData.add(fd3);
        FootballData fdRes = new FootballData();
        Calculator cal = new Calculator();
        fdRes = (FootballData) cal.analyseFootballData(allFootballData);
        assertEquals("Team 1", fdRes.getTeamname());
        assertEquals(55, fdRes.getGoalsScored());
        assertEquals(49, fdRes.getGoalsAgainst());
        assertEquals(6, fdRes.getDifference());
    }
}
