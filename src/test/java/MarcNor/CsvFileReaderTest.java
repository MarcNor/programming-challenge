package MarcNor;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import controller.CsvFileReader;
import model.FootballData;
import model.WeatherData;

public class CsvFileReaderTest 
{
    @Test
    //Test if the weather data file exist and includes the right data
    public void testFileOutputWeather()
    {
    	CsvFileReader test = new CsvFileReader();
		List<String> res = test.readFile("WeatherData");
		assertEquals("Day,MxT,MnT,AvT,AvDP,1HrP TPcpn,PDir,AvSp,Dir,MxS,SkyC,MxR,Mn,R AvSLP", res.get(0));
    }
    
    @Test
    //Test if the football data file exist and includes the right data
    public void testFileOutputFootball()
    {
    	CsvFileReader test = new CsvFileReader();
		List<String> res = test.readFile("FootballData");
		assertEquals("Team,Games,Wins,Losses,Draws,Goals,Goals Allowed,Points", res.get(0));
    }
    
    @Test
    //Test if the read data for the weather data will be filtered (number of day, min and max Temprature) correctly and a correct model is build
    public void testFilteredDataResultWeather() {
    	CsvFileReader reader = new CsvFileReader();
        List<String> result = reader.readFile("WeatherData");
        List<Object> resultObjects = reader.filterCsvDataWeather(result);
        WeatherData wd = new WeatherData();
        wd = (WeatherData) resultObjects.get(0);
        int numOfDay = wd.getNumberOfDay();
        int minTemp = wd.getMinTemp();
        int maxTemp = wd.getMaxTemp();
        assertEquals(1 , numOfDay);
        assertEquals(59, minTemp);
        assertEquals(88, maxTemp);
    }
    
    @Test
    //Test if the read data for the football data will be filtered (team name, goals scored and goals against) correctly and a correct model is build
    public void testFilteredDataResultFootball() {
    	CsvFileReader reader = new CsvFileReader();
        List<String> result = reader.readFile("FootballData");
        List<Object> resultObjects = reader.filterCsvDataFootball(result);
        FootballData fd = new FootballData();
        fd = (FootballData) resultObjects.get(0);
        String teamname = fd.getTeamname();
        int goalsScored = fd.getGoalsScored();
        int goalsAgainst = fd.getGoalsAgainst();
        assertEquals("Arsenal" , teamname);
        assertEquals(79, goalsScored);
        assertEquals(36, goalsAgainst);
    }
}
